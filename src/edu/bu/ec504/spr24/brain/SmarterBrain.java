package edu.bu.ec504.spr24.brain;

import edu.bu.ec504.spr24.brain.Board.Pos;
import javax.swing.*;

/**
 * A smarter brain, for you to produce.
 */
public class SmarterBrain extends Brain {
    public SmarterBrain() {
        super();
    }

    @Override
    public String myName() {
        return null;
    }

    @Override
    Pos nextMove() {
        JOptionPane.showMessageDialog(null, "Please insert brain here!");
        return null;
    }

}
